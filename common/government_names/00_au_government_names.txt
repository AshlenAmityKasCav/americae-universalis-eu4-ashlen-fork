# For specific combinations of culture, religion and other such triggers
# Will pick the first valid one it finds in list

#Heir and consort titles are generally kept simple for clarity unless there is something special in particular that can be used.

place_holder_papal_holder = {
	rank = {
		1 = PLACE_HOLDER
		2 = PLACE_HOLDER
		3 = PLACE_HOLDER
	}
	
	ruler_male = {
		1 = PLACE_HOLDER_MAN
		2 = PLACE_HOLDER_MAN
		3 = PLACE_HOLDER_MAN
	}
	
	ruler_female  ={
		1 = PLACE_HOLDER_WOMAN
		2 = PLACE_HOLDER_WOMAN
		3 = PLACE_HOLDER_WOMAN
	}
	
	trigger = {
		has_reform = place_holder_reform
	}
}

}